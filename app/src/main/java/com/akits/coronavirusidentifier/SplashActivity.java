package com.akits.coronavirusidentifier;

//import androidx.appcompat.app.AppCompatActivity;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.Bundle;
//import android.os.Handler;
//import android.provider.Settings;
//import android.widget.Toast;
//
//import com.nabinbhandari.android.permissions.PermissionHandler;
//import com.nabinbhandari.android.permissions.Permissions;
//
//import java.util.ArrayList;
//
//import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
//import static android.Manifest.permission.ACCESS_FINE_LOCATION;
//import static android.Manifest.permission.ACCESS_NETWORK_STATE;
//import static android.Manifest.permission.ACCESS_WIFI_STATE;
//public class SplashActivity extends AppCompatActivity {
//    String[] permissions = new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION,ACCESS_NETWORK_STATE,ACCESS_WIFI_STATE};
//    String rationale = "Please provide permission to proceed ...";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash);
//
//        if(checkConnection(SplashActivity.this))
//        {
//
//        }
//        else
//        {
//            new AlertDialog.Builder(this).
//                    setTitle("Enable GPS")
//                    .setMessage("Please Change GPS settings to high accuracy")
//                    .setPositiveButton("K", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
//                            startActivityForResult(intent, 210);
//                        }
//                    })
//                    .setNegativeButton("Nah", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    }).show();
//        }
//
//
//        Permissions.Options options = new Permissions.Options()
//                .setRationaleDialogTitle("Info")
//                .setSettingsDialogTitle("Warning");
//
//        Permissions.check(SplashActivity.this, permissions, rationale, options, new PermissionHandler() {
//            @Override
//            public void onGranted() {
//                // do your task.
//
//                new Handler().postDelayed(new Runnable() {
//
//
//                    @Override
//
//                    public void run() {
//
//                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
//
//                        startActivity(i);
//
//                        // close this activity
//
//                        finish();
//
//                    }
//
//                }, 3*1000);
//
//
//            }
//
//            @Override
//            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
//                // permission denied, block the feature.
//                Toast.makeText(context, "Need Permissions to continue", Toast.LENGTH_SHORT).show();
//                finish();
//            }
//        });
//
//    }

//
//public boolean Internet() {
//        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
//        NetworkInfo info = cm.getActiveNetworkInfo();
//        if (info != null && info.isConnected()) {
//        return true;
//        }
//        return false;
//
//        }
//public void statusCheck() {
//final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//        buildAlertMessageNoGps();
//
//        }
//        }
//
//
//
//    public static boolean checkConnection(Context context) {
//
//        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
//
//        if (activeNetworkInfo != null) { // connected to the internet
//            //Toast.makeText(context, activeNetworkInfo.getTypeName(), Toast.LENGTH_SHORT).show();
//
//            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
//                // connected to wifi
//                return true;
//            } else if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
//                // connected to the mobile provider's data plan
//                return true;
//            }
//        }
//        return false;
//    }
//}
//


// > > > >    >  > >  > > > . > > >?> > /.

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class SplashActivity extends AppCompatActivity {
    public SharedPreferences getPrefs;
    SharedPreferences.Editor e;

    private final Handler handler = new Handler();
    boolean isGPSLocation, isNetworkLocation;

    private LocationManager locationManager;
    Animation r;
    protected static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 1;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 100;
    private final int REQUEST_LOCATION_HIGH_ACCURACY = 215;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final LocationManager mListener = (LocationManager) getSystemService(Context.LOCATION_SERVICE);



        ActivityCompat.requestPermissions(SplashActivity.this, new String[]
                {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getCityByLocation();
                }
                return;
            }
        }
    }


    void getCityByLocation() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        if (Internet()) {
            Thread splash = new Thread() {
                public void run() {
                    try {


                        sleep(3000);


                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);

                        finish();

                    } catch (Exception e) {
                    }
                }
            };
            // start thread
            splash.start();


        } else {


            new android.app.AlertDialog.Builder(SplashActivity.this).
                    setTitle("No Internet Connection")
                    .setCancelable(false)
                    .setMessage("No Internet Connection. Make Sure that Your  Data Service is Enable,then Try Again")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));


                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                            finish();
                        }
                    }).show();


//                AlertDialog.Builder adb = new AlertDialog.Builder(this);
//                adb.setTitle("No Internet Connection");
//                adb.setMessage("No Internet Connection. Make Sure that Your  Data Service is Enable,then Try Again ");
////            adb.setIcon(R.drawable.exit);
//                adb.setCancelable(false);
//
//                adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//
//                    }
//                });
//                adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//
//
//                    }
//                });
//
//                AlertDialog alert11 = adb.create();
//                alert11.show();


//


        }
    }

    public boolean Internet() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            return true;
        }
        return false;

    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    public int getLocationMode(Context context) {
        try {
            return Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            return -1;
        }
    }

    private void buildAlertMessageNoGps() {


        new android.app.AlertDialog.Builder(SplashActivity.this).
                setTitle("No Location Access")
                .setCancelable(false)
                .setMessage("Your GPS Service Seems  Disabled To Use This App With Full Features Enable Settings and Restart App !")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                }).show();

    }


//
//
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage("Your GPS Service Seems  Disabled To Use This App With Full Features Enable Settings and Restart App !")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, final int id) {
//
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, final int id) {
//
//                    }
//                });
//        final AlertDialog alert = builder.create();
//        alert.show();


}

