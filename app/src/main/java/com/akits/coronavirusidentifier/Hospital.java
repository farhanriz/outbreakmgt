package com.akits.coronavirusidentifier;

public class Hospital {
    public String name;
    public int id;

    public Hospital() {
    }

    public Hospital(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return this.name;            // What to display in the Spinner list.
    }
}
