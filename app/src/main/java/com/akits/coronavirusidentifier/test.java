package com.akits.coronavirusidentifier;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akits.rooom.AlertDB;
import com.akits.rooom.AlertDataSource;
import com.akits.rooom.AlertRepository;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

public class test {
}


//
//package com.akits.coronavirusidentifier;
//
//        import androidx.annotation.NonNull;
//        import androidx.appcompat.app.AppCompatActivity;
//        import androidx.fragment.app.FragmentManager;
//
//        import androidx.lifecycle.Lifecycle;
//        import androidx.lifecycle.LifecycleOwner;
//        import androidx.lifecycle.Observer;
//        import androidx.recyclerview.widget.DividerItemDecoration;
//        import androidx.recyclerview.widget.LinearLayoutManager;
//        import androidx.recyclerview.widget.RecyclerView;
//
//        import android.app.AlertDialog;
//        import android.app.ProgressDialog;
//        import android.content.Context;
//        import android.content.DialogInterface;
//        import android.content.Intent;
//        import android.content.SharedPreferences;
//        import android.os.Bundle;
//        import android.provider.Settings;
//        import android.util.Log;
//        import android.view.View;
//        import android.widget.ImageView;
//        import android.widget.TextView;
//        import android.widget.Toast;
//
//        import com.akits.rooom.AlertDB;
//        import com.akits.rooom.AlertDataSource;
//        import com.akits.rooom.AlertRepository;
//        import com.android.volley.Request;
//        import com.android.volley.RequestQueue;
//        import com.android.volley.Response;
//        import com.android.volley.VolleyError;
//        import com.android.volley.toolbox.JsonArrayRequest;
//        import com.android.volley.toolbox.Volley;
//        import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
//        import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
//        import com.nabinbhandari.android.permissions.PermissionHandler;
//        import com.nabinbhandari.android.permissions.Permissions;
//
//        import org.json.JSONArray;
//        import org.json.JSONException;
//        import org.json.JSONObject;
//
//        import java.text.SimpleDateFormat;
//        import java.util.ArrayList;
//        import java.util.Date;
//        import java.util.List;
//        import java.util.Locale;
//        import java.util.Objects;
//
//        import io.reactivex.ObservableEmitter;
//        import io.reactivex.ObservableOnSubscribe;
//        import io.reactivex.android.schedulers.AndroidSchedulers;
//        import io.reactivex.annotations.Nullable;
//        import io.reactivex.disposables.Disposable;
//        import io.reactivex.functions.Action;
//        import io.reactivex.functions.Consumer;
//        import io.reactivex.schedulers.Schedulers;
//        import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
//public class MainActivity extends AppCompatActivity {
//    FlowingDrawer mDrawer;
//    ImageView toggle, delete;
//    List<Alert> alertList;
//    RecyclerView recyclerView;
//    AlertRepository noteRepository;
//    TextView textVie;
//
//    private RecyclerView.Adapter adapter;
//
//    public static String PREFS_NAME = "time_stamp";
//    public static String PREFS_TIME = "current_time";
//
//    SharedPreferences settings;
//    SharedPreferences.Editor editor;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        recyclerView = findViewById(R.id.oye);
//        delete = findViewById(R.id.delete);
//        textVie = findViewById(R.id.empty_view);
//        recyclerView.setItemAnimator(new SlideInLeftAnimator());
//        alertList = new ArrayList<>();
//
//        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
//        editor = settings.edit();
//        AlertDB alertDB = AlertDB.getInstance(this);
//        noteRepository = AlertRepository.getInstance(AlertDataSource.getInstance(alertDB.AlertDao()));
//        Date date = new Date(System.currentTimeMillis());
//
//        long millis = date.getTime();
//        Log.e("Farhan", " " + millis + "   " + getTimes());
//        long date1 = getTimes();
//
//        if (millis - date1 > 7200000) {
//            dellete();
//            setTimes();
//            getData();
//
//        } else {
//            loadData();
//
//        }
//
//
//        delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                new AlertDialog.Builder(MainActivity.this).
//                        setTitle("Delete All")
//                        .setMessage("Are you sure you want to delete all news ?")
//                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dellete(); }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }}).show(); }});
//        toggle = findViewById(R.id.toggle);
//        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
//        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
//        setupMenu();
//        toggle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDrawer.toggleMenu();
//            }
//        });
//        mDrawer.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
//            @Override
//            public void onDrawerStateChange(int oldState, int newState) {
//                if (newState == ElasticDrawer.STATE_CLOSED) {
//                    Log.i("MainActivity", "Drawer STATE_CLOSED");
//                }
//            }
//
//            @Override
//            public void onDrawerSlide(float openRatio, int offsetPixels) {
//                Log.i("MainActivity", "openRatio=" + openRatio + " ,offsetPixels=" + offsetPixels);
//            }
//        });
//
//
//    }
//
//
//
//
//
//
//
//
//
//
//
//    private void setupMenu() {
//        FragmentManager fm = getSupportFragmentManager();
//        menulist_fragment mMenuFragment = (menulist_fragment) fm.findFragmentById(R.id.id_container_menu);
//        if (mMenuFragment == null) {
//            mMenuFragment = new menulist_fragment();
//            fm.beginTransaction().add(R.id.id_container_menu, mMenuFragment).commit();
//        }
//    }
//
//    private void getData() {
//
//        String url = "https://outbreak2.comocrm.com/api/newsalerts/news/0";
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Loading...");
//        progressDialog.show();
//
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//                for (int i = 0; i < response.length(); i++) {
//                    try {
//                        JSONObject jsonObject = null;
//                        try {
//                            jsonObject = response.getJSONObject(i);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        final Alert movie = new Alert();
//                        movie.setTitle(jsonObject != null ? jsonObject.getString("title") : null);
//                        assert jsonObject != null;
//                        movie.setDesc(jsonObject.getString("description"));
//                        movie.setType("news");
//                        Log.e("volley", "onResponse: " + movie.desc);
//
//
//                        // call the insert here
//
//
////                        AlertRepository noteRepository = new AlertRepository(getApplicationContext());
////
////                        noteRepository.insertAlert(movie);
//
//
//                        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
//                            @Override
//                            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
//          /*  String note = ed_note.getText().toString();
//            String latlon =CommonData.curent_location.getLatitude() + "," + CommonData.curent_location.getLongitude();
//            Notes fish = new Notes(dateToStr, latlon, Float.valueOf(ed_length.getText().toString()), Float.valueOf(ed_weight.getText().toString()), ed_species.getText().toString(), note, imageFilePath);
//            //               fishList.add(fish);*/
//
//                                noteRepository.insertAlert(movie);
//                                emitter.onComplete();
//
//                            }
//                        }).observeOn(AndroidSchedulers.mainThread())
//                                .subscribeOn(Schedulers.io())
//                                .subscribe(
//                                        new Consumer() {
//                                            @Override
//                                            public void accept(Object o) throws Exception {
//                                                // Toast.makeText(MainActivity.this, "Updated !", Toast.LENGTH_SHORT).show();
//
//                                                //clearRecord();
//
//                                            }
//
//                                        }, new Consumer<Throwable>() {
//                                            @Override
//                                            public void accept(Throwable throwable) throws Exception {
//                                                Toast.makeText(getApplicationContext(), "Unable to update new Log due to " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
//                                            }
//                                        }, new Action() {
//                                            @Override
//                                            public void run() throws Exception {
//                                                //    Toast.makeText(MainActivity.this, "Updated !", Toast.LENGTH_SHORT).show();
//
//                                                //   clearRecord();
//
//                                            }
//                                        });
//
//
//                        loadData();
//
//                        //  alertList.add(movie);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        progressDialog.dismiss();
//                    }
//                }
//
//
//                // save all data here brother
//
//
//                //   adapter.notifyDataSetChanged();
//                progressDialog.dismiss();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("Volley", error.toString());
//                progressDialog.dismiss();
//            }
//        });
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(jsonArrayRequest);
//    }
//
//    private void loadData() {
//
//        Disposable disposable = noteRepository.getAlertId("news")
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer<List<Alert>>() {
//                    @Override
//                    public void accept(List<Alert> fishInformations) throws Exception {
//                        onGetAllFishSuccess(fishInformations);
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Log.e("HAQHAQ", throwable.getMessage());
//                    }
//                });
//
//        //compositeDisposable.add(disposable);
//    }
//
//    private void onGetAllFishSuccess(List<Alert> fishInformations) {
//
//
//        if (fishInformations.isEmpty()) {
//            recyclerView.setVisibility(View.GONE);
//            textVie.setVisibility(View.VISIBLE);
//        } else {
//            recyclerView.setVisibility(View.VISIBLE);
//            textVie.setVisibility(View.GONE);
//            adapter = new AlertAdapter(getApplicationContext(), fishInformations);
//            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
//
//            recyclerView.setHasFixedSize(true);
//            recyclerView.setLayoutManager(linearLayoutManager);
//            recyclerView.addItemDecoration(dividerItemDecoration);
//            recyclerView.setAdapter(adapter);
//            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
//        }
//    }
//
//    private void dellete() {
//
//
//        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
//            @Override
//            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
//
//                noteRepository.deleteAll("news");
//
//                emitter.onComplete();
//
//            }
//        }).observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer() {
//                    @Override
//                    public void accept(Object o) throws Exception {
//
//
//                        Toast.makeText(MainActivity.this, "Cleared !", Toast.LENGTH_LONG).show();
//
//                    }
//
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Toast.makeText(getApplicationContext(), "Unable to delete it due to " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                }, new Action() {
//                    @Override
//                    public void run() throws Exception {
//
//
//                    }
//                });
//
//
//    }
//
//    private void setTimes() {
//
//
//        Date date = new Date(System.currentTimeMillis()); //or simply new Date();
//        long millis = date.getTime();
//
//        getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
//                .edit()
//                .putLong(PREFS_TIME, millis)
//                .commit();
//
//    }
//
//    public long getTimes() {
//
////
//        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
//        editor = settings.edit();
////        Date myDate = new Date(settings.getLong("time", 0));
////        return myDate.getTime();
//
//
//        return settings.getLong(PREFS_TIME, 0);
//    }
//
//
//}
