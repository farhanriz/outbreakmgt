package com.akits.coronavirusidentifier;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;

import java.util.Date;

import static com.akits.coronavirusidentifier.MainActivity.city;
import static com.akits.coronavirusidentifier.MainActivity.toogleMenu;

/**
 * Created by mxn on 2016/12/13.
 * MenuListFragment
 */

public class menulist_fragment extends Fragment {

    NavigationView vNavigation;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    public static String PREFS_NAME = "time_stampaa";
    public static String PREFS_TIME = "current_timeaa";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container,
                false);

        vNavigation = (NavigationView) view.findViewById(R.id.vNavigation);
        settings = getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();





        vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                int id = item.getItemId();


                if (id == R.id.alerts) {
                    // Handle the camera action
                    toogleMenu();
                    if(city!=null)
                    startActivity(new Intent(getContext(), AlertActivity.class));
                    else
                        Toast.makeText(getActivity(), "Cannot start restart app !", Toast.LENGTH_SHORT).show();



                } else if (id == R.id.news) {
                    toogleMenu();
                    if(city!=null)
                    startActivity(new Intent(getContext(), GeneralAlertActivity.class));
                    else
                        Toast.makeText(getActivity(), "Cannot start restart app !", Toast.LENGTH_SHORT).show();

                }  else if (id == R.id.newsa) {
                    toogleMenu();
                    Date date = new Date(System.currentTimeMillis());

                    long millis = date.getTime();
                    Log.e("Farhan is at menu", " " + millis + "   " + getTimes());
                    long date1 = getTimes();
                    if (millis - date1 > 7200000) {

                        new android.app.AlertDialog.Builder(getContext()).
                                setTitle("Select Language")
                                .setCancelable(false)
                                .setMessage("Choose your preferred language !")
                                .setPositiveButton("English", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                        startActivity(new Intent(getContext(), SubmitFormActivity.class));


                                    }
                                }).setNegativeButton("اردو", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                startActivity(new Intent(getContext(), SubmitFormActivityy.class));


                            }
                        })
                                .show();




                    }
                    else
                    {
                        showTOast("You already submitted a request \n You are not allowed to post any request before 2 hours");
                    }



                }

                return true;

            }
        });

        return view;
    }
    void showTOast(String message) {
        new android.app.AlertDialog.Builder(getContext()).
                setTitle("Already Submitted")
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();


                    }
                })
                .show();
    }

    public long getTimes() {

//
        settings = getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
//        Date myDate = new Date(settings.getLong("time", 0));
//        return myDate.getTime();


        return settings.getLong(PREFS_TIME, 0);
    }


}



// be back and check the time values