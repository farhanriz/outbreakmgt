package com.akits.coronavirusidentifier;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitClientOutbreak {

    public static final String BASE_URLL = "https://outbreak2.comocrm.com/api/";
    public static Retrofit mRetrofit = null;

    public RetrofitClientOutbreak(){

    }
    public static Retrofit getRetrofitey() {

        if (mRetrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            mRetrofit = new  Retrofit.Builder()
                    .baseUrl(BASE_URLL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();

            return mRetrofit;
        }
        else {
            return mRetrofit;
        }


    }

}

