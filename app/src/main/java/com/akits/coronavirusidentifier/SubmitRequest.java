package com.akits.coronavirusidentifier;


import com.google.gson.annotations.SerializedName;

public class SubmitRequest {

    @SerializedName("success")
    private String success;

    public String getSuccess() {
        return success;
    }

    @Override
    public String toString() {
        return
                "SubmitResponse{" +
                        "success = '" + success + '\'' +
                        "}";
    }
}