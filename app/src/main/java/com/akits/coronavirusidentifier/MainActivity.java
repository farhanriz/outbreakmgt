package com.akits.coronavirusidentifier;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.FragmentManager;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akits.coronavirusidentifier.Retrofit.APIService;
import com.akits.coronavirusidentifier.Retrofit.ApiServicee;
import com.akits.coronavirusidentifier.Retrofit.RetrofitClient;
import com.akits.coronavirusidentifier.response.AddressComponentsItem;
import com.akits.rooom.AlertDB;
import com.akits.rooom.AlertDataSource;
import com.akits.rooom.AlertRepository;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import authoritydmc.UpdateApp;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<LocationSettingsResult> {
    public static FlowingDrawer mDrawer;
    ImageView toggle, delete;
    List<Alert> alertList;
    RecyclerView recyclerView;
    public static String city;
    public static String address;
    private int interval_seconds = 600;

    List<Hospital> hospitalList;
    public static List<Address> addresses;

    AlertRepository noteRepository;
    TextView textVie;

    private RecyclerView.Adapter adapter;

    public static String PREFS_NAME = "time_stamp";
    public static String PREFS_TIME = "current_time";

    public static String PREFS_NAMEE = "city";
    public static String PREFS_TIMEE = "city";

    public static String PREFS_NAMELE = "lat";
    public static String PREFS_TIMELE = "lat";

    public static String PREFS_NAMELO = "lon";
    public static String PREFS_TIMELO = "lon";


    SharedPreferences settings;
    SharedPreferences.Editor editor;


    SharedPreferences settingscity;
    SharedPreferences.Editor editorcity;


    SharedPreferences settingslat;
    SharedPreferences.Editor editorlat;


    SharedPreferences settingslon;
    SharedPreferences.Editor editorlon;

    APIService apiService;


    private static final int REQUEST_CHECK_SETTINGS = 214;
    private static final int REQUEST_ENABLE_GPS = 516;
    private final int REQUEST_LOCATION_PERMISSION = 210;
    private final int REQUEST_LOCATION_HIGH_ACCURACY = 215;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationSettingsRequest mLocationSettingsRequest;
    /* For Google Fused API */
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private Context context;

    public static void toogleMenu() {
        mDrawer.toggleMenu();
    }
    private String getCurrentVersion() {
        //get the current version number and name

        String versionName = "";
        int versionCode = -1;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.oye);
        delete = findViewById(R.id.delete);
        textVie = findViewById(R.id.empty_view);
        recyclerView.setItemAnimator(new SlideInLeftAnimator());
        alertList = new ArrayList<>();
        UpdateApp.checkupdate(MainActivity.this, getCurrentVersion(), true);
        context = this;

        new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        Retrofit mRetrofit = RetrofitClient.getRetrofite();



        apiService = mRetrofit.create(APIService.class);




        mFusedLocationClient = new FusedLocationProviderClient(this);
        mLocationRequest = new LocationRequest();

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (getLocationMode(context) == 2)
            mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);


        mLocationRequest.setInterval(interval_seconds * 1000);
        mLocationRequest.setFastestInterval(interval_seconds * 1000);

        if (getLocationMode(this) != 3) {
            new AlertDialog.Builder(this).
                    setTitle("Enable GPS On High Accuracy")
                    .setMessage("Please change gps settings to high accuracy")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, REQUEST_LOCATION_HIGH_ACCURACY);

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    }).show();
        } else {
            buildGoogleApiClient();
        }


        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();


        settingscity = getSharedPreferences(PREFS_NAMEE, Context.MODE_PRIVATE);
        editorcity = settings.edit();

        settingslat = getSharedPreferences(PREFS_NAMELE, Context.MODE_PRIVATE);
        editorlat = settings.edit();


        settingslon = getSharedPreferences(PREFS_NAMELO, Context.MODE_PRIVATE);
        editorlon = settings.edit();


        AlertDB alertDB = AlertDB.getInstance(this);
        noteRepository = AlertRepository.getInstance(AlertDataSource.getInstance(alertDB.AlertDao()));
        Date date = new Date(System.currentTimeMillis());

        long millis = date.getTime();
  //      Log.e("Farhan", " " + millis + "   " + getTimes());
        long date1 = getTimes();

        if (millis - date1 > 7200000) {
            dellete();
            setTimes();
            getData();

        } else {
            loadData();

        }


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(MainActivity.this).
                        setTitle("Delete All")
                        .setMessage("Are you sure you want to delete all news ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dellete();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });
        toggle = findViewById(R.id.toggle);
        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        setupMenu();
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.toggleMenu();
            }
        });
        mDrawer.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (newState == ElasticDrawer.STATE_CLOSED) {
                    Log.i("MainActivity", "Drawer STATE_CLOSED");
                }
            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {
                Log.i("MainActivity", "openRatio=" + openRatio + " ,offsetPixels=" + offsetPixels);
            }
        });


    }


    private void setupMenu() {
        FragmentManager fm = getSupportFragmentManager();
        menulist_fragment mMenuFragment = (menulist_fragment) fm.findFragmentById(R.id.id_container_menu);
        if (mMenuFragment == null) {
            mMenuFragment = new menulist_fragment();
            fm.beginTransaction().add(R.id.id_container_menu, mMenuFragment).commit();
        }
    }

    private void getData() {

        String url = "https://outbreak2.comocrm.com/api/newsalerts/news/0";
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = response.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        final Alert movie = new Alert();
                        movie.setTitle(jsonObject != null ? jsonObject.getString("title") : null);
                        assert jsonObject != null;
                        movie.setDesc(jsonObject.getString("description"));
                        movie.setType("news");
                        Log.e("volley", "onResponse: " + movie.desc);


                        // call the insert here


//                        AlertRepository noteRepository = new AlertRepository(getApplicationContext());
//
//                        noteRepository.insertAlert(movie);


                        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
                            @Override
                            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
          /*  String note = ed_note.getText().toString();
            String latlon =CommonData.curent_location.getLatitude() + "," + CommonData.curent_location.getLongitude();
            Notes fish = new Notes(dateToStr, latlon, Float.valueOf(ed_length.getText().toString()), Float.valueOf(ed_weight.getText().toString()), ed_species.getText().toString(), note, imageFilePath);
            //               fishList.add(fish);*/

                                noteRepository.insertAlert(movie);
                                emitter.onComplete();

                            }
                        }).observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(
                                        new Consumer() {
                                            @Override
                                            public void accept(Object o) throws Exception {
                                                // Toast.makeText(MainActivity.this, "Updated !", Toast.LENGTH_SHORT).show();

                                                //clearRecord();

                                            }

                                        }, new Consumer<Throwable>() {
                                            @Override
                                            public void accept(Throwable throwable) throws Exception {
                                                Toast.makeText(getApplicationContext(), "Unable to update new Log due to " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }, new Action() {
                                            @Override
                                            public void run() throws Exception {
                                                //    Toast.makeText(MainActivity.this, "Updated !", Toast.LENGTH_SHORT).show();

                                                //   clearRecord();

                                            }
                                        });


                        loadData();

                        //  alertList.add(movie);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }


                // save all data here brother


                //   adapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    private void loadData() {

        Disposable disposable = noteRepository.getAlertId("news")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Alert>>() {
                    @Override
                    public void accept(List<Alert> fishInformations) throws Exception {
                        onGetAllFishSuccess(fishInformations);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e("HAQHAQ", throwable.getMessage());
                    }
                });

        //compositeDisposable.add(disposable);
    }

    private void onGetAllFishSuccess(List<Alert> fishInformations) {


        if (fishInformations.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            textVie.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            textVie.setVisibility(View.GONE);
            adapter = new AlertAdapter(getApplicationContext(), fishInformations);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());

            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setAdapter(adapter);
            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
        }
    }

    private void dellete() {


        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {

                noteRepository.deleteAll("news");

                emitter.onComplete();

            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                    @Override
                    public void accept(Object o) throws Exception {


                        Toast.makeText(MainActivity.this, "Cleared !", Toast.LENGTH_LONG).show();

                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(getApplicationContext(), "Unable to delete it due to " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {


                    }
                });


    }

    private void setTimes() {


        Date date = new Date(System.currentTimeMillis()); //or simply new Date();
        long millis = date.getTime();

        getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .edit()
                .putLong(PREFS_TIME, millis)
                .commit();

    }

    public long getTimes() {

//
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
//        Date myDate = new Date(settings.getLong("time", 0));
//        return myDate.getTime();


        return settings.getLong(PREFS_TIME, 0);
    }

    public int getLocationMode(Context context) {
        try {
            return Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            return -1;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        connectGoogleClient();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Helper.showLog("Location Received");
                mCurrentLocation = locationResult.getLastLocation();
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);

                final List<Address> dataList = getAddresses(getApplicationContext(),
                        locationResult.getLastLocation().getLatitude(),
                        locationResult.getLastLocation().getLongitude());
                if (dataList != null) {
                    city = dataList.get(0).getLocality();
               //     Toast.makeText(context, "Data List", Toast.LENGTH_SHORT).show();

                    getSharedPreferences(PREFS_NAMEE, MODE_PRIVATE)
                            .edit()
                            .putString(PREFS_TIMEE, city)
                            .commit();


                    getSharedPreferences(PREFS_NAMELE, MODE_PRIVATE)
                            .edit()
                            .putString(PREFS_TIMELE, locationResult.getLastLocation().getLatitude() + "")
                            .commit();


                    getSharedPreferences(PREFS_NAMELO, MODE_PRIVATE)
                            .edit()
                            .putString(PREFS_TIMELO, locationResult.getLastLocation().getLongitude() + "")
                            .commit();
                    address = dataList.get(0).getAddressLine(0);
                    //Toast.makeText(context, ""+, Toast.LENGTH_SHORT).show();
                    // getData(city); start





                    // >>> end



                } else {
                    apiService.getCityName(locationResult.getLastLocation().getLatitude() + "," +
                                    locationResult.getLastLocation().getLongitude(), "true",
                            "AIzaSyAzYrrRUd0OXerh2hXxEwStB5kaf7mNSNc").
                            subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<com.akits.coronavirusidentifier.response.Response>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onNext(com.akits.coronavirusidentifier.response.Response response) {
                                    List<AddressComponentsItem> addressComponentsItems = response.getResults().get(0).getAddressComponents();
                                    city = addressComponentsItems.get(2).getLongName();
                                 //   Toast.makeText(context, "Data List 1", Toast.LENGTH_SHORT).show();
                                    getSharedPreferences(PREFS_NAMEE, MODE_PRIVATE)
                                            .edit()
                                            .putString(PREFS_TIMEE, city)
                                            .commit();


                                    getSharedPreferences(PREFS_NAMELE, MODE_PRIVATE)
                                            .edit()
                                            .putString(PREFS_TIMELE, locationResult.getLastLocation().getLatitude() + "")
                                            .commit();


                                    getSharedPreferences(PREFS_NAMELO, MODE_PRIVATE)
                                            .edit()
                                            .putString(PREFS_TIMELO, locationResult.getLastLocation().getLongitude() + "")
                                            .commit();
                                    address = response.getResults().get(0).getFormattedAddress();
                                    // getData(city);
                                    //Toast.makeText(SubmitFormActivity.this, "Hi"+city, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onComplete() {

                                }
                            });
                }

//

                Log.e("Farhaan", "onLocationResult: " + locationResult.getLastLocation().getLatitude() +
                        locationResult.getLastLocation().getLongitude());


//                final String city = curentcity(locationResult.getLastLocation().getLatitude(),
//                                locationResult.getLastLocation().getLongitude());

                // Toast.makeText(context, city, Toast.LENGTH_SHORT).show();


                onLocationChanged(mCurrentLocation);
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);

            }
        };
    }

    private void connectGoogleClient() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SUCCESS) {
            mGoogleApiClient.connect();
        } else {
            int REQUEST_GOOGLE_PLAY_SERVICE = 988;
            googleAPI.getErrorDialog(this, resultCode, REQUEST_GOOGLE_PLAY_SERVICE);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(60 * 1000);
        mLocationRequest.setFastestInterval(15 * 1000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (getLocationMode(context) == 2)
            mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();

        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Helper.showLog("GPS Success");
                        requestLocationUpdate();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException rae = (ResolvableApiException) e;
                            rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sie) {
                            Helper.showLog("PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Helper.showLog("Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                }
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Helper.showLog("checkLocationSettings -> onCanceled");
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        connectGoogleClient();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        String latitude = String.valueOf(location.getLatitude());
        String longitude = String.valueOf(location.getLongitude());

        if (latitude.equalsIgnoreCase("0.0") && longitude.equalsIgnoreCase("0.0")) {
            requestLocationUpdate();
        } else {
            //Perform Your Task with LatLong
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdate() {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Helper.showLog("User allow to access location. Request Location Update");
                    requestLocationUpdate();
                    break;
                case Activity.RESULT_CANCELED:
                    Helper.showLog("User denied to access location.");
                    openGpsEnableSetting();
                    break;
            }
        } else if (requestCode == REQUEST_ENABLE_GPS) {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (!isGpsEnabled) {
                openGpsEnableSetting();
            } else {
                requestLocationUpdate();
            }
        } else if (requestCode == REQUEST_LOCATION_HIGH_ACCURACY) {
            if (getLocationMode(MainActivity.this) != 3) {


                new android.app.AlertDialog.Builder(MainActivity.this).
                        setTitle("GPS Error ")
                        .setCancelable(false)
                        .setMessage("GPS Still not in high Accuracy \n Try Again and Restart App !")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                dialog.dismiss();
                                finish();
                                //    mFusedLocationClient.removeLocationUpdates(mLocationCallback);


                            }
                        })
                        .show();


            } else {
                buildGoogleApiClient();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                Helper.showLog("User Allowed Permission Build Google Client");
                buildGoogleApiClient();
            } else {
                Helper.showLog("User Denied Permission");
            }
        }

    }

    private void openGpsEnableSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, REQUEST_ENABLE_GPS);
    }

    @Override
    protected void onDestroy() {
        //Remove location update callback here

        if (mCurrentLocation != null && mLocationCallback != null && mLocationRequest != null && mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        super.onDestroy();
    }


    static public List<Address> getAddresses(Context mContext, Double latitude, Double longitude) {
        Geocoder geocoder;
        addresses = new ArrayList<>();
        geocoder = new Geocoder(mContext, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}