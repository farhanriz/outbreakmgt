package com.akits.coronavirusidentifier.Retrofit;

import com.akits.coronavirusidentifier.response.Response;


import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

//    @GET("{latitude},{longitude}")
//    Observable<Result> getCityName(
//            @Field("latlng") String latitude,
//            @Field("sensor") String sensor,
//            @Field("key") String key
//    );

    @GET("json")
    Observable<Response> getCityName(
            @Query("latlng") String latitude,
            @Query("sensor") String sensor,
            @Query("key") String key
    );
}
