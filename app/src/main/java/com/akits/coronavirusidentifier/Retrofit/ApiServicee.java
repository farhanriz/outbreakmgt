package com.akits.coronavirusidentifier.Retrofit;

import com.akits.coronavirusidentifier.SubmitRequest;
import com.akits.coronavirusidentifier.response.Response;

import org.json.JSONObject;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiServicee {


    @POST("create-appointment")
    Observable<SubmitRequest> Submitrequest(
            @Query("hospital") String hospitalid,
            @Query("city") String city,
            @Query("name") String name,
            @Query("email") String email,
            @Query("address") String address,
            @Query("long") String lon,
            @Query("lat") String lat,
            @Query("phone_no") String phone,
            @Query("age") String age,
            @Query("sex") String sex,
            @Query("no_family_member_flu") String no_family_member_flu,
            @Query("no_family_member_age_50") String no_family_member_age_50,
            @Query("any_diseases") String any_diseases,
            @Query("diffulty_breathing") String diffulty_breathing,
            @Query("fever") String fever,
            @Query("dry_cough") String dry_cough,
            @Query("recently_travelled_abroad") String recently_travelled_abroad,
            @Query("met_recently_travelled_abroad") String met_recently_travelled_abroad,
            @Query("family_met_recently_travelled_abroad") String family_met_recently_travelled_abroad,
            @Query("family_recently_travelled_abroad") String family_recently_travelled_abroad,
            @Query("family_attend_public_gathering") String family_attend_public_gathering,
            @Query("color") String color


    );
}
