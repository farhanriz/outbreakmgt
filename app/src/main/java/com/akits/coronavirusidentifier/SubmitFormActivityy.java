package com.akits.coronavirusidentifier;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;



// save the sensative data in sharedprefs like lat lon and city and address

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.akits.coronavirusidentifier.Retrofit.ApiServicee;
import com.akits.coronavirusidentifier.Retrofit.RetrofitClient;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static com.akits.coronavirusidentifier.MainActivity.city;

public class SubmitFormActivityy extends AppCompatActivity {
    TextInputEditText editText1, editText2, editText3, editText4;
    EditText editText5, editText6, editText7;
    Spinner spsex;
    Button submit;
    boolean fla;
    public static String PREFS_NAME = "time_stampaa";
    public static String PREFS_TIME = "current_timeaa";


    public static String PREFS_NAMEE = "city";
    public static String PREFS_TIMEE = "city";

    public static String PREFS_NAMELE = "lat";
    public static String PREFS_TIMELE = "lat";

    public static String PREFS_NAMELO = "lon";
    public static String PREFS_TIMELO = "lon";


    SharedPreferences settingscity;
    SharedPreferences.Editor editorcity;
    ImageView togglel;
    ProgressDialog progressDialog,progressDialoga;


    SharedPreferences settingslat;
    SharedPreferences.Editor editorlat;
    ApiServicee apiServicee;


    SharedPreferences settingslon;
    SharedPreferences.Editor editorlon;

    SharedPreferences settings;
    SharedPreferences.Editor editor;

    SearchableSpinner userSpinner;
    ArrayAdapter<String> adapter;
    ArrayAdapter<Hospital> userAdapter;
    RadioGroup radioGroup1, radioGroup2, radioGroup3, radioGroup4, radioGroup5, radioGroup6, radioGroup7, radioGroup8, radioGroup9;
    String res;
    List<Hospital> hospitalList;

    int countred, countgreen, countorrange = 0;
    String hospitalid, city, name, email, address, phone, age, sex, no_family_member_flu, no_family_member_age_50, any_diseases, diffulty_breathing, fever, dry_cough, recently_travelled_abroad, met_recently_travelled_abroad, family_met_recently_travelled_abroad, family_recently_travelled_abroad, family_attend_public_gathering, color;
    String lat, lon;

    public boolean Internet() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            return true;
        }
        return false;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_submit_form_activityy);
        togglel=findViewById(R.id.toggle);
        togglel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubmitFormActivityy.this.finish();
            }
        });
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();


        settingscity = getSharedPreferences(PREFS_NAMEE, Context.MODE_PRIVATE);
        editorcity = settings.edit();
        if (settingscity.getString(PREFS_TIMEE, "") == null) {
            showTOast("No City found !");
        } else {
            city = settingscity.getString(PREFS_TIMEE, "");
        }
        settingslat = getSharedPreferences(PREFS_NAMELE, Context.MODE_PRIVATE);
        editorlat = settings.edit();

        if (settingslat.getString(PREFS_TIMELE, "") == null) {
            showTOast("No latitude found !");
        } else {
            lat = settingslat.getString(PREFS_TIMELE, "");
        }
        settingslon = getSharedPreferences(PREFS_NAMELO, Context.MODE_PRIVATE);
        editorlon = settings.edit();

        if (settingslon.getString(PREFS_TIMELO, "") == null) {
            showTOast("No longitude found !");
        } else {
            lon = settingslon.getString(PREFS_TIMELO, "");
        }


        Date date = new Date(System.currentTimeMillis());

        long millis = date.getTime();
        Log.e("Farhan", "at start " + millis + "   " + getTimes());
        long date1 = getTimes();


        Retrofit retrofit= RetrofitClientOutbreak.getRetrofitey();

        apiServicee=retrofit.create(ApiServicee.class);








        editText1 = findViewById(R.id.name);
        editText2 = findViewById(R.id.phone);
        submit = findViewById(R.id.submit);
        editText3 = findViewById(R.id.email);
        editText4 = findViewById(R.id.address);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Submitting request...");

        radioGroup1 = findViewById(R.id.radioGroupotherdes);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupotherdesyes:
                        countred++;
                        countorrange++;
                        any_diseases = "Yes";
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupotherdesno:
                        any_diseases = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });


        radioGroup2 = findViewById(R.id.radioGroupbreath);

        radioGroup3 = findViewById(R.id.radioGroupfever);

        radioGroup4 = findViewById(R.id.radioGroupcough);

        radioGroup5 = findViewById(R.id.radioGrouptravelabroad);

        radioGroup6 = findViewById(R.id.radioGroupmetabroad);

        radioGroup7 = findViewById(R.id.radioGroupfamilymetabroad);

        radioGroup8 = findViewById(R.id.radioGroupfamilyaboroad);

        radioGroup9 = findViewById(R.id.radioGrouppublic);

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupbreathyes:
                        diffulty_breathing = "Yes";
                        countorrange++;
                        countred++;

                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupbreathno:
                        diffulty_breathing = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });


        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupfeveryes:
                        fever = "Yes";
                        countred++;
                        countorrange++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupfeverno:
                        fever = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupcoughyes:
                        dry_cough = "Yes";
                        countred++;
                        countorrange++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupcoughno:
                        dry_cough = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGrouptravelabroadyes:
                        recently_travelled_abroad = "Yes";
                        countred++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGrouptravelabroadno:
                        recently_travelled_abroad = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupmetabroadyes:
                        met_recently_travelled_abroad = "Yes";
                        countred++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupmetabroadno:
                        met_recently_travelled_abroad = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupfamilymetabroadyes:
                        family_met_recently_travelled_abroad = "Yes";
                        countred++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupfamilymetabroadno:
                        family_met_recently_travelled_abroad = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupfamilyaboroadyes:
                        family_recently_travelled_abroad = "Yes";
                        countred++;

                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupfamilyaboroadno:
                        family_recently_travelled_abroad = "No";
                        countgreen++;

                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGrouppublicyes:
                        family_attend_public_gathering = "Yes";
                        countred++;
                        countorrange++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGrouppublicno:
                        family_attend_public_gathering = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        editText5 = findViewById(R.id.spfamilyq);
        editText6 = findViewById(R.id.spfamily);
        editText7 = findViewById(R.id.spage);


        hospitalList = new ArrayList<>();
        // Toast.makeText(this, "Hi   "+city, Toast.LENGTH_SHORT).show();


        spsex = findViewById(R.id.spsex);
        userSpinner = findViewById(R.id.spinner);


        userAdapter = new ArrayAdapter<>(SubmitFormActivityy.this, R.layout.spinner_item, getData(city));


        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userSpinner.setTitle("Select nearest hospital");
        userSpinner.setPositiveButton("OK");
        spsex.setSelection(0);
        userSpinner.setSelection(0);
        userSpinner.setAdapter(userAdapter);


        String[] itemssex = new String[]{"Male", "Female", "Not Specified"};

        adapter = new ArrayAdapter<>(SubmitFormActivityy.this, R.layout.spinner_item, itemssex);

//set the spinners adapter to the previously created one.
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spsex.setAdapter(adapter);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (millis - date1 > 7200000) {
                    if (Internet()) {

                        checkEmptyFieild();
                    } else {
                        new android.app.AlertDialog.Builder(SubmitFormActivityy.this).
                                setTitle("No Internet Connection")
                                .setCancelable(false)
                                .setMessage("No Internet Connection. Make Sure that Your  Data Service is Enable,then Try Again")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));


                                        finish();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();

                                        finish();
                                    }
                                }).show();
                    }

                } else {

                    showTOast("آپ نے پہلے ہی ایک درخواست پیش کی ہے\n" +
                            " آپ کو 2 گھنٹے سے پہلے کوئی درخواست پوسٹ کرنے کی اجازت نہیں ہے");

                }


            }
        });


        spsex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                sex = itemssex[position];
                //   Toast.makeText(SubmitFormActivityy.this, ""+itemssex[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Toast.makeText(SubmitFormActivityy.this, "" + hospitalList.get(position).id, Toast.LENGTH_SHORT).show();
                //Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                hospitalid = hospitalList.get(position).id + "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void checkEmptyFieild() {

        if (editText1.getText() == null || editText1.getText().length() < 1) {

            editText1.setError("اپنا پورا نام درج کریں");
        } else if (editText2.getText() == null || editText2.getText().length() < 1) {

            editText2.setError("فون نمبر درج کریں!");

        } else if (editText4.getText() == null || editText4.getText().length() < 1) {
            editText4.setError("اپنے گھر کا پتہ درج کریں!");


        } else if (editText5.getText() == null || editText5.getText().length() < 1) {
            editText5.setError("فلو میں مبتلا کنبہ کے افراد کی تعداد درج کریں! ");

        } else if (editText6.getText() == null || editText6.getText().length() < 1) {
            editText6.setError("50 سال سے زیادہ عمر کے کنبہ کے افراد کی تعداد درج کریں!");

        } else if (editText7.getText() == null || editText7.getText().length() < 1) {

            editText7.setError("براہ کرم اپنی عمر لکھیں!");

        } else if (hospitalid == null) {
            showTOast("فہرست سے اپنے قریبی اسپتال کا انتخاب کریں!");
        } else if (any_diseases == null) {
            showTOast("کسی بھی بیماری کے لئے اختیارات کا انتخاب کریں!");
        } else if (diffulty_breathing == null) {
            showTOast("سانس لینے میں دشواری کے  آپشنز کا انتخاب کریں !");
        } else if (fever == null) {
            showTOast("بخار کے لئے آپشن کا انتخاب کریں!");
        } else if (dry_cough == null) {
            showTOast("خشک کھانسی کے آپشن کا انتخاب کریں!");
        } else if (recently_travelled_abroad == null) {
            showTOast("حال ہی میں بیرون ملک سفر کے لئے آپشن کا انتخاب کریں!");
        } else if (met_recently_travelled_abroad == null) {
            showTOast("بیرون ملک سے حال ہی میں کسی سے ملنے کے  آپشن کا انتخاب کریں!");
        } else if (family_met_recently_travelled_abroad == null) {
            showTOast("اگر خاندان کے کسی فرد سے کسی نے حال ہی میں بیرون ملک سفر کیا ہو تو اس کا اآپشن منتخب کریں!");
        } else if (family_recently_travelled_abroad == null) {
            showTOast("اگر خاندان کے کسی فرد نے حال ہی میں بیرون ملک سفر کیا تو اآپشن کا انتخاب کریں!");
        } else if (family_attend_public_gathering == null) {
            showTOast("اگر آپ یا کنبہ کے کوئی فرد عوامی اجتماع میں شریک ہوا تو اختیارات کا انتخاب کریں !");
        } else {


            name = editText1.getText().toString();
            phone = editText2.getText().toString();
            email = Objects.requireNonNull(editText3.getText()).toString() + " ";
            address = editText4.getText().toString();
            no_family_member_flu = editText5.getText().toString();
            no_family_member_age_50 = editText6.getText().toString();
            age = editText7.getText().toString();
            if (countred > countorrange && countred > countgreen) {
                color = "Red";
            } else if (countorrange > countred && countorrange > countgreen) {
                color = "Orange";
            } else {
                color = "Green";

            }




//            if (getDataa(name, phone, email, address, age, sex, hospitalid, no_family_member_age_50, no_family_member_flu, color, lat, lon)) {
//
//

//
//
//
//            } else {
//
//              //  Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
//
//                setTimes();

//            }
//


            getResults();


        }


    }


    void getResults()
    {
        progressDialoga = new ProgressDialog(this);
        progressDialoga.setMessage("درخواست جمع کرائی جارہی ہے . . .");
        progressDialoga.show();



        apiServicee.Submitrequest(hospitalid,city,name,email,address,lon,lat,phone,age,sex,no_family_member_flu,no_family_member_age_50,any_diseases,diffulty_breathing,fever,dry_cough,recently_travelled_abroad,met_recently_travelled_abroad,family_met_recently_travelled_abroad,family_recently_travelled_abroad,family_attend_public_gathering,color).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SubmitRequest>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(SubmitRequest aBoolean) {
                        res =aBoolean.getSuccess();
                        progressDialoga.dismiss();
                        if(res.equals("true"))
                        {
                            setTimes();
                            new android.app.AlertDialog.Builder(SubmitFormActivityy.this).
                                    setTitle("درخواست مکمل ہوگئی")
                                    .setCancelable(false)
                                    .setMessage("ہم آپ کے دروازے پر آپ کے پاس آئیں گے. کسی بھی صحت مرکز کا دورہ نہ کریں. محفوظ رہنے کے لئے گھر رہیں  !")
                                    .setPositiveButton("ٹھیک ہے", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();
                                            SubmitFormActivityy.this.finish();

                                        }
                                    })
                                    .show();
                        }
                        else
                        {
                            new android.app.AlertDialog.Builder(SubmitFormActivityy.this).
                                    setTitle("درخواست مکمل نہیں ہوئی !")
                                    .setCancelable(false)
                                    .setMessage("کچھ دیر بعد دوبارہ کوشش کریں !")
                                    .setPositiveButton("ٹھیک ہے", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();
                                            SubmitFormActivityy.this.finish();


                                        }
                                    })
                                    .show();
                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialoga.dismiss();

                        new android.app.AlertDialog.Builder(SubmitFormActivityy.this).
                                setTitle("درخواست مکمل نہیں ہوئی !")
                                .setCancelable(false)
                                .setMessage("کچھ دیر بعد دوبارہ کوشش کریں !")
                                .setPositiveButton("ٹھیک ہے", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                        SubmitFormActivityy.this.finish();


                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onComplete() {


                    }
                });
      //  return res;

    }


    private List<Hospital> getData(String city) {


        String url = "https://outbreak2.comocrm.com/api/cityhospitals/" + city;
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = response.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Hospital movie = new Hospital();
                        assert jsonObject != null;
                        movie.id = jsonObject.getInt("id");
                        movie.name = jsonObject.getString("name");

                        hospitalList.add(movie);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
                progressDialog.dismiss();


                //   adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
//        Log.e("volley", hospitalList.get(0).name);
        return hospitalList;
    }


    private boolean getDataa(String name, String phone, String email, String address, String age, String sex, String hospitalid, String no_family_member_age_50, String no_family_member_flu, String color, String lat, String lon) {



        String url = "https://outbreak2.comocrm.com/api/create-appointment?hospital=" + hospitalid + "&city=" + city + "&name=" + name + "&email=" + email + "&address=" + address + "&long=" + lon + "&lat=" + lat + "&phone_no=" + phone + "&age=" + age + "&sex=" + sex + "&no_family_member_flu=" + no_family_member_flu + "&no_family_member_age_50=" + no_family_member_age_50 + "&any_diseases=" + any_diseases + "&diffulty_breathing=" + diffulty_breathing + "&fever=" + fever + "&dry_cough=" + dry_cough + "&recently_travelled_abroad=" + recently_travelled_abroad + "&met_recently_travelled_abroad=" + met_recently_travelled_abroad + "&family_met_recently_travelled_abroad=" + family_met_recently_travelled_abroad + "&family_recently_travelled_abroad=" + family_recently_travelled_abroad + "&family_attend_public_gathering=" + family_attend_public_gathering + "&color=" + color;
        Log.e("Farhaaan", "Farhaaan:    " + url);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(this);
//        String url = "http://your_url.com/abc.php"; // <----enter your post url here
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Farhaan", "onResponse: here i ma "+response );
                JSONObject myObject = null;
                try {
                    myObject = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                assert myObject != null;

                try {
                    fla = myObject.getBoolean("success");
                    Log.e("Farhaan", "onResponse: here i ma "+fla );



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();

                }
                progressDialog.dismiss();

                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                progressDialog.dismiss();
            }
        }) {

        };


        MyRequestQueue.add(MyStringRequest);




        Log.e("volley", hospitalList.get(0).name);
        return fla;
    }

    void showTOast(String message) {
        new android.app.AlertDialog.Builder(SubmitFormActivityy.this).
                setTitle("نامکمل معلومات")
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton("ٹھیک ہے", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();


                    }
                })
                .show();
    }

    private void setTimes() {


        Date date = new Date(System.currentTimeMillis()); //or simply new Date();
        long millis = date.getTime();
        Log.e("Farhan", "Farhan at set times: "+millis );

        getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .edit()
                .putLong(PREFS_TIME, millis)
                .commit();

    }



    public long getTimes() {

//
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
//        Date myDate = new Date(settings.getLong("time", 0));
//        return myDate.getTime();


        return settings.getLong(PREFS_TIME, 0);
    }
}





