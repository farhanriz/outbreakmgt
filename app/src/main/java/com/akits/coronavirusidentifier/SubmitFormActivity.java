package com.akits.coronavirusidentifier;

// save the sensative data in sharedprefs like lat lon and city and address

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.akits.coronavirusidentifier.Retrofit.ApiServicee;
import com.akits.coronavirusidentifier.Retrofit.RetrofitClient;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static com.akits.coronavirusidentifier.MainActivity.city;

public class SubmitFormActivity extends AppCompatActivity {
    TextInputEditText editText1, editText2, editText3, editText4;
    EditText editText5, editText6, editText7;
    Spinner spsex;
    Button submit;
    boolean fla;
    public static String PREFS_NAME = "time_stampaa";
    public static String PREFS_TIME = "current_timeaa";


    public static String PREFS_NAMEE = "city";
    public static String PREFS_TIMEE = "city";

    public static String PREFS_NAMELE = "lat";
    public static String PREFS_TIMELE = "lat";

    public static String PREFS_NAMELO = "lon";
    public static String PREFS_TIMELO = "lon";


    SharedPreferences settingscity;
    SharedPreferences.Editor editorcity;
    ImageView togglel;
    ProgressDialog progressDialog,progressDialoga;


    SharedPreferences settingslat;
    SharedPreferences.Editor editorlat;
    ApiServicee apiServicee;


    SharedPreferences settingslon;
    SharedPreferences.Editor editorlon;

    SharedPreferences settings;
    SharedPreferences.Editor editor;

    SearchableSpinner userSpinner;
    ArrayAdapter<String> adapter;
    ArrayAdapter<Hospital> userAdapter;
    RadioGroup radioGroup1, radioGroup2, radioGroup3, radioGroup4, radioGroup5, radioGroup6, radioGroup7, radioGroup8, radioGroup9;
String res;
    List<Hospital> hospitalList;

    int countred, countgreen, countorrange = 0;
    String hospitalid, city, name, email, address, phone, age, sex, no_family_member_flu, no_family_member_age_50, any_diseases, diffulty_breathing, fever, dry_cough, recently_travelled_abroad, met_recently_travelled_abroad, family_met_recently_travelled_abroad, family_recently_travelled_abroad, family_attend_public_gathering, color;
    String lat, lon;

    public boolean Internet() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            return true;
        }
        return false;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_submit_form);
togglel=findViewById(R.id.toggle);
togglel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        SubmitFormActivity.this.finish();
    }
});
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();


        settingscity = getSharedPreferences(PREFS_NAMEE, Context.MODE_PRIVATE);
        editorcity = settings.edit();
        if (settingscity.getString(PREFS_TIMEE, "") == null) {
            showTOast("No City found !");
        } else {
            city = settingscity.getString(PREFS_TIMEE, "");
        }
        settingslat = getSharedPreferences(PREFS_NAMELE, Context.MODE_PRIVATE);
        editorlat = settings.edit();

        if (settingslat.getString(PREFS_TIMELE, "") == null) {
            showTOast("No latitude found !");
        } else {
            lat = settingslat.getString(PREFS_TIMELE, "");
        }
        settingslon = getSharedPreferences(PREFS_NAMELO, Context.MODE_PRIVATE);
        editorlon = settings.edit();

        if (settingslon.getString(PREFS_TIMELO, "") == null) {
            showTOast("No longitude found !");
        } else {
            lon = settingslon.getString(PREFS_TIMELO, "");
        }


        Date date = new Date(System.currentTimeMillis());

        long millis = date.getTime();
        Log.e("Farhan", "at start " + millis + "   " + getTimes());
        long date1 = getTimes();


        Retrofit retrofit= RetrofitClientOutbreak.getRetrofitey();

        apiServicee=retrofit.create(ApiServicee.class);








        editText1 = findViewById(R.id.name);
        editText2 = findViewById(R.id.phone);
        submit = findViewById(R.id.submit);
        editText3 = findViewById(R.id.email);
        editText4 = findViewById(R.id.address);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Submitting request...");

        radioGroup1 = findViewById(R.id.radioGroupotherdes);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupotherdesyes:
                        countred++;
                        countorrange++;
                        any_diseases = "Yes";
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupotherdesno:
                        any_diseases = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });


        radioGroup2 = findViewById(R.id.radioGroupbreath);

        radioGroup3 = findViewById(R.id.radioGroupfever);

        radioGroup4 = findViewById(R.id.radioGroupcough);

        radioGroup5 = findViewById(R.id.radioGrouptravelabroad);

        radioGroup6 = findViewById(R.id.radioGroupmetabroad);

        radioGroup7 = findViewById(R.id.radioGroupfamilymetabroad);

        radioGroup8 = findViewById(R.id.radioGroupfamilyaboroad);

        radioGroup9 = findViewById(R.id.radioGrouppublic);

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupbreathyes:
                        diffulty_breathing = "Yes";
                        countorrange++;
                        countred++;

                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupbreathno:
                        diffulty_breathing = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });


        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupfeveryes:
                        fever = "Yes";
                        countred++;
                        countorrange++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupfeverno:
                        fever = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupcoughyes:
                        dry_cough = "Yes";
                        countred++;
                        countorrange++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupcoughno:
                        dry_cough = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGrouptravelabroadyes:
                        recently_travelled_abroad = "Yes";
                        countred++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGrouptravelabroadno:
                        recently_travelled_abroad = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupmetabroadyes:
                        met_recently_travelled_abroad = "Yes";
                        countred++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupmetabroadno:
                        met_recently_travelled_abroad = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupfamilymetabroadyes:
                        family_met_recently_travelled_abroad = "Yes";
                        countred++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupfamilymetabroadno:
                        family_met_recently_travelled_abroad = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGroupfamilyaboroadyes:
                        family_recently_travelled_abroad = "Yes";
                        countred++;

                        // do operations specific to this selection
                        break;
                    case R.id.radioGroupfamilyaboroadno:
                        family_recently_travelled_abroad = "No";
                        countgreen++;

                        // do operations specific to this selection
                        break;

                }

            }
        });

        radioGroup9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioGrouppublicyes:
                        family_attend_public_gathering = "Yes";
                        countred++;
                        countorrange++;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGrouppublicno:
                        family_attend_public_gathering = "No";
                        countgreen++;
                        // do operations specific to this selection
                        break;

                }

            }
        });

        editText5 = findViewById(R.id.spfamilyq);
        editText6 = findViewById(R.id.spfamily);
        editText7 = findViewById(R.id.spage);


        hospitalList = new ArrayList<>();
        // Toast.makeText(this, "Hi   "+city, Toast.LENGTH_SHORT).show();


        spsex = findViewById(R.id.spsex);
        userSpinner = findViewById(R.id.spinner);


        userAdapter = new ArrayAdapter<>(SubmitFormActivity.this, R.layout.spinner_item, getData(city));


        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userSpinner.setTitle("Select nearest hospital");
        userSpinner.setPositiveButton("OK");
        spsex.setSelection(0);
        userSpinner.setSelection(0);
        userSpinner.setAdapter(userAdapter);


        String[] itemssex = new String[]{"Male", "Female", "Not Specified"};

        adapter = new ArrayAdapter<>(SubmitFormActivity.this, R.layout.spinner_item, itemssex);

//set the spinners adapter to the previously created one.
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spsex.setAdapter(adapter);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (millis - date1 > 7200000) {
                    if (Internet()) {

                        checkEmptyFieild();
                    } else {
                        new android.app.AlertDialog.Builder(SubmitFormActivity.this).
                                setTitle("No Internet Connection")
                                .setCancelable(false)
                                .setMessage("No Internet Connection. Make Sure that Your  Data Service is Enable,then Try Again")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));


                                        finish();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();

                                        finish();
                                    }
                                }).show();
                    }

                } else {

                    showTOast("You already submitted a request \n You are not allowed to post any request before 2 hours");

                }


            }
        });


        spsex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                sex = itemssex[position];
                //   Toast.makeText(SubmitFormActivity.this, ""+itemssex[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Toast.makeText(SubmitFormActivity.this, "" + hospitalList.get(position).id, Toast.LENGTH_SHORT).show();
                //Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                hospitalid = hospitalList.get(position).id + "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void checkEmptyFieild() {

        if (editText1.getText() == null || editText1.getText().length() < 1) {
            showTOast("Enter your full name !");

        } else if (editText2.getText() == null || editText2.getText().length() < 1) {
            showTOast("Enter phone number !");

        } else if (editText4.getText() == null || editText4.getText().length() < 1) {
            showTOast("Enter your home address !");
        } else if (editText5.getText() == null || editText5.getText().length() < 1) {
            showTOast("Enter number of family members suffering from flu !");
        } else if (editText6.getText() == null || editText6.getText().length() < 1) {
            showTOast("Enter number of family members age above 50 !");
        } else if (editText7.getText() == null || editText7.getText().length() < 1) {
            showTOast("Please enter your age !");

        } else if (hospitalid == null) {
            showTOast("Select your nearest hospital from list !");
        } else if (any_diseases == null) {
            showTOast("Choose the options for any diseases !");
        } else if (diffulty_breathing == null) {
            showTOast("Choose the options for difficulty in breathing !");
        } else if (fever == null) {
            showTOast("Choose the options for fever !");
        } else if (dry_cough == null) {
            showTOast("Choose the options for dry cough !");
        } else if (recently_travelled_abroad == null) {
            showTOast("Choose the options for recently travelled abroad !");
        } else if (met_recently_travelled_abroad == null) {
            showTOast("Choose the options for recently meet anyone from abroad!");
        } else if (family_met_recently_travelled_abroad == null) {
            showTOast("Choose the options for if any family member meet anyone recently travelled abroad !");
        } else if (family_recently_travelled_abroad == null) {
            showTOast("Choose the options if any family member recently travelled abroad !");
        } else if (family_attend_public_gathering == null) {
            showTOast("Choose the options if anyone from you or family attended public gathering !");
        } else {


            name = editText1.getText().toString();
            phone = editText2.getText().toString();
            email = Objects.requireNonNull(editText3.getText()).toString() + " ";
            address = editText4.getText().toString();
            no_family_member_flu = editText5.getText().toString();
            no_family_member_age_50 = editText6.getText().toString();
            age = editText7.getText().toString();
            if (countred > countorrange && countred > countgreen) {
                color = "Red";
            } else if (countorrange > countred && countorrange > countgreen) {
                color = "Orange";
            } else {
                color = "Green";

            }




//            if (getDataa(name, phone, email, address, age, sex, hospitalid, no_family_member_age_50, no_family_member_flu, color, lat, lon)) {

//
//
//
//            } else {
//
//              //  Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
//
//                setTimes();
//                new android.app.AlertDialog.Builder(SubmitFormActivity.this).
//                        setTitle("Request Submitted Successfully")
//                        .setCancelable(false)
//                        .setMessage("Stay Home to Stay Safe \n Don't visit any health center unnecessarily \n we will contact you shortly \n keep checking the alert section !")
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
//                                dialog.dismiss();
//                                SubmitFormActivity.this.finish();
//
//
//                            }
//                        })
//                        .show();
//            }
//


            getResults();


        }


    }


    void getResults()
    {
        progressDialoga = new ProgressDialog(this);
        progressDialoga.setMessage("Submitting request...");
        progressDialoga.show();



        apiServicee.Submitrequest(hospitalid,city,name,email,address,lon,lat,phone,age,sex,no_family_member_flu,no_family_member_age_50,any_diseases,diffulty_breathing,fever,dry_cough,recently_travelled_abroad,met_recently_travelled_abroad,family_met_recently_travelled_abroad,family_recently_travelled_abroad,family_attend_public_gathering,color).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SubmitRequest>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(SubmitRequest aBoolean) {
                        res =aBoolean.getSuccess();

                        if(res.equals("true")) {
                            setTimes();

                            new android.app.AlertDialog.Builder(SubmitFormActivity.this).
                                    setTitle("Request Submitted Successfully")
                                    .setCancelable(false)
                                    .setMessage("Stay Home to Stay Safe \n Don't visit any health center unnecessarily \n we will contact you shortly \n keep checking the alert section !")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();
                                            SubmitFormActivity.this.finish();


                                        }
                                    })
                                    .show();

                        }
                        else
                        {


                            new android.app.AlertDialog.Builder(SubmitFormActivity.this).
                                    setTitle("Request Not Submitted")
                                    .setCancelable(false)
                                    .setMessage("Your request cannot processed \n Try again later !")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();
                                            SubmitFormActivity.this.finish();


                                        }
                                    })
                                    .show();
                        }
                        progressDialoga.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {


                    }

                    @Override
                    public void onComplete() {


                    }
                });
       // return res;

    }


    private List<Hospital> getData(String city) {


        String url = "https://outbreak2.comocrm.com/api/cityhospitals/" + city;
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = response.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Hospital movie = new Hospital();
                        assert jsonObject != null;
                        movie.id = jsonObject.getInt("id");
                        movie.name = jsonObject.getString("name");

                        hospitalList.add(movie);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
                progressDialog.dismiss();


                //   adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
//        Log.e("volley", hospitalList.get(0).name);
        return hospitalList;
    }


    private boolean getDataa(String name, String phone, String email, String address, String age, String sex, String hospitalid, String no_family_member_age_50, String no_family_member_flu, String color, String lat, String lon) {



        String url = "https://outbreak2.comocrm.com/api/create-appointment?hospital=" + hospitalid + "&city=" + city + "&name=" + name + "&email=" + email + "&address=" + address + "&long=" + lon + "&lat=" + lat + "&phone_no=" + phone + "&age=" + age + "&sex=" + sex + "&no_family_member_flu=" + no_family_member_flu + "&no_family_member_age_50=" + no_family_member_age_50 + "&any_diseases=" + any_diseases + "&diffulty_breathing=" + diffulty_breathing + "&fever=" + fever + "&dry_cough=" + dry_cough + "&recently_travelled_abroad=" + recently_travelled_abroad + "&met_recently_travelled_abroad=" + met_recently_travelled_abroad + "&family_met_recently_travelled_abroad=" + family_met_recently_travelled_abroad + "&family_recently_travelled_abroad=" + family_recently_travelled_abroad + "&family_attend_public_gathering=" + family_attend_public_gathering + "&color=" + color;
        Log.e("Farhaaan", "Farhaaan:    " + url);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(this);
//        String url = "http://your_url.com/abc.php"; // <----enter your post url here
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Farhaan", "onResponse: here i ma "+response );
                JSONObject myObject = null;
                try {
                     myObject = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                assert myObject != null;

                try {
                    fla = myObject.getBoolean("success");
                    Log.e("Farhaan", "onResponse: here i ma "+fla );



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();

                }
                progressDialog.dismiss();

                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                progressDialog.dismiss();
            }
        }) {

        };


        MyRequestQueue.add(MyStringRequest);




        Log.e("volley", hospitalList.get(0).name);
        return fla;
    }

    void showTOast(String message) {
        new android.app.AlertDialog.Builder(SubmitFormActivity.this).
                setTitle("Incomplete information")
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();


                    }
                })
                .show();
    }

    private void setTimes() {


        Date date = new Date(System.currentTimeMillis()); //or simply new Date();
        long millis = date.getTime();
        Log.e("Farhan", "Farhan at set times: "+millis );

        getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .edit()
                .putLong(PREFS_TIME, millis)
                .commit();

    }



    public long getTimes() {

//
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
//        Date myDate = new Date(settings.getLong("time", 0));
//        return myDate.getTime();


        return settings.getLong(PREFS_TIME, 0);
    }
}




