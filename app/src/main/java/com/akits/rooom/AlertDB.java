package com.akits.rooom;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import android.content.Context;

import com.akits.coronavirusidentifier.Alert;

@Database(entities = Alert.class, version = 1)
public abstract class AlertDB extends RoomDatabase {
    public static final String DATABASE_NAME = "alert-database";
    private static final int DATABASE_VERSION = 1;

    public abstract AlertDao AlertDao();

    public static AlertDB mInstance;

    public static AlertDB getInstance(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context, AlertDB.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return mInstance;
    }


}