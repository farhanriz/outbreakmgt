package com.akits.rooom;


import com.akits.coronavirusidentifier.Alert;

import java.util.List;

import io.reactivex.Flowable;

public class AlertRepository implements IAlertDataSource {

    private IAlertDataSource mLocalDataSource;
    private static AlertRepository mInstance;

    public AlertRepository(IAlertDataSource mLocalDataSource) {
        this.mLocalDataSource = mLocalDataSource;
    }

    public static AlertRepository getInstance(IAlertDataSource mLocalDataSource) {
        if (mInstance == null) {
            mInstance = new AlertRepository(mLocalDataSource);
        }
        return mInstance;

    }


    @Override
    public Flowable<List<Alert>> getAlertId(String type) {
        return mLocalDataSource.getAlertId(type);
    }

    @Override
    public void deleteAll(String type) {
        mLocalDataSource.deleteAll(type);

    }

    @Override
    public void insertAlert(Alert... histories) {
        mLocalDataSource.insertAlert(histories);


    }

    @Override
    public void updateAlert(Alert... histories) {

        mLocalDataSource.updateAlert(histories);
    }

    @Override
    public void deleteAlert(Alert histories) {
        mLocalDataSource.deleteAlert(histories);

    }
}

