package com.akits.rooom;


import com.akits.coronavirusidentifier.Alert;

import java.util.List;
;
import io.reactivex.Flowable;

import java.util.List;

import io.reactivex.Flowable;

public interface IAlertDataSource {


    Flowable<List<Alert>> getAlertId(String type);

    void deleteAll(String type);

    void insertAlert(Alert... histories);

    void updateAlert(Alert... histories);

    void deleteAlert(Alert histories);

}