package com.akits.rooom;


import android.content.Context;
import android.widget.Toast;


import com.akits.coronavirusidentifier.Alert;

import java.util.List;


import io.reactivex.Flowable;

import java.util.List;

import io.reactivex.Flowable;

public class AlertDataSource implements IAlertDataSource {

    Context context;
    private AlertDao AlertDao;
    private static AlertDataSource mInstance;

    public AlertDataSource(AlertDao AlertDao) {
        this.AlertDao = AlertDao;
    }

    public static AlertDataSource getInstance(AlertDao AlertDao) {
        if (mInstance == null) {
            mInstance = new AlertDataSource(AlertDao);
        }
        return mInstance;
    }


    @Override
    public Flowable<List<Alert>> getAlertId(String type) {
        return AlertDao.getAlertId(type);
    }


    @Override
    public void deleteAll(String type) {
        AlertDao.deleteAll(type);


    }


    @Override
    public void insertAlert(Alert... histories) {
        AlertDao.insertAlert(histories);

    }

    @Override
    public void updateAlert(Alert... histories) {

        AlertDao.updateAlert(histories);
    }

    @Override
    public void deleteAlert(Alert Alert) {
        AlertDao.deleteAlert(Alert);
    }


}
