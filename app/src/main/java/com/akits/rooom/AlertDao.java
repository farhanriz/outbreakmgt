package com.akits.rooom;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import androidx.room.Dao;


import com.akits.coronavirusidentifier.Alert;

import java.util.List;


import io.reactivex.Flowable;

@Dao
public interface AlertDao {

    @Query("SELECT * FROM Alert WHERE type =:type")
    Flowable<List<Alert>> getAlertId(String type);

    @Query("DELETE  FROM Alert WHERE type=:type")
    void deleteAll(String type);

    @Insert
    void insertAlert(Alert... histories);

    @Update
    void updateAlert(Alert... histories);

    @Delete
    void deleteAlert(Alert histories);


}


